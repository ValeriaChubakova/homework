print("Чубакова Валерия 14124")
print("Данная программа производит сортировку строк пузырьком")

try:
    nf=input("Введите имя входного файла:")
    f=open(nf, encoding="UTF-8").readlines()
    if len(f)<10 or len(f)>150:
        print("Недопустимое количество строк")
        exit()
    for n in f:
        if len(n)<5 or len(n)>80:
            print("Недопустимое количество символов в строке")
            exit()          
    nff=input("Введите имя выходного файла")
    ff=open(nff, "w")
    for i in range (len(f)-1):
        for j in range (len(f)-i-1):
            if f[j]>f[j+1]:
                f[j], f[j+1]=f[j+1], f[j]
    for s in f:
        ff.write(s)
    ff.close()
except FileNotFoundError:
    print(f"Запрашиваемый файл {nf} не найден")
    
