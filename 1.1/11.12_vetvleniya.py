#задание 1
correct_yaer=False
while not correct_yaer:
    try:
        year = int(input('Введите год: '))
        if year <=30000:
            if(year%4 == 0 and year%100!=0) or (year%400==0):
                print("YES")
            else:
                print("NO")
        else:
            print('Слишком большое число')
        correct_yaer=True
    except ValueError or NameError:
        print("Число не соответсвует году. Попробуйте еще раз:")

#задание 2
correct_input=False
while not correct_input:
    try:
        print('Введите три натуральных числа')
        x=int(input('Первое число: '))
        y=int(input('Второе число: '))
        z=int(input('Третье число: '))
        print(max(x,y,z))
        correct_input=True
    except ValueError or NameError:
        print("Числа не натуральные. Попробуйте еще раз:")

