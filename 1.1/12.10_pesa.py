roles={}
counter=0
num_roles=0
roles['Слова автора']=[]
f=open('roles.txt','r',encoding='utf-8')
for str_role in f.readlines():
    if counter==0:
        counter+=1
        continue
    if (str_role!='textLines:\n'):
        roles[str_role[:len(str_role)-1]]=[]
    if (str_role=="textLines:\n"):
        num_roles=len(roles)
        break
    
counter=1
currole=" "
t=open('roles.txt','r',encoding='utf-8')
for str_role in t.readlines():
    if counter > num_roles+2:
        r_rep=str_role.split(': ')
        if r_rep[0] in roles.keys():
            replics=roles[r_rep[0]]
            replics.append(str(counter-num_roles-2)+') '+r_rep[1])
            roles[r_rep[0]]=replics
            currole=r_rep[0]
        else:
            roles[currole].append(r_rep[0])
        if str_role.find("(")!=-1:
            in_open=str_role.find('(')
            in_close=str_role.find(')')
            replics=str_role[in_open+1:in_close]
            roles['Слова автора'].append(replics)
    counter+=1
    
for i in roles:
    print(f"{i}: ")
    for x in roles[i]:
        print(x)
    print()
