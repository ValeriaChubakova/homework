correct_input=False
while not correct_input:
    try:
        x=float(input('Введите x: '))
        op=input('Выберите операцию (+,-,*,/,//,%): ')
        y=float(input('Введите y: '))
        correct_input=True
    except ValueError or NameError:
        print("x или y не цифры. Попробуйте еще раз:")

if op=="+":
    print(x,"+",y,'=', x+y)
    print(f'Результат {x+y}')
elif op=="-":
    print(x,"-",y,'=', x-y)
    print(f'Результат {x-y}')
elif op=="*":
    print(x,"*",y,'=', x*y)
    print(f'Результат {x*y}')
elif op=="/":
    print(x,"/",y,'=', x/y)
    print(f'Результат {x/y}')
else:
    print('Операция введена не верно')

    
