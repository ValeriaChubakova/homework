from tkinter import *
from tkinter import ttk
from tkinter.filedialog import asksaveasfile
from tkinter.ttk import Combobox
from tkinter.ttk import Radiobutton

anketa = Tk()
anketa.geometry('800x400') 
frm = ttk.Frame(anketa, padding=10)
frm.grid()
ttk.Label(frm, text="Анкетирование").grid(column=0, row=0)

ttk.Label(frm, text="Имя и Фамилия").grid(column=0, row=1)
num1_en = ttk.Entry(frm)
num1_en.grid(column=1,row=1)
   

ttk.Label(frm, text="Выберите ваш любимый жанр").grid(column=0, row=5)
Radiobutton(frm, text='ужасы', value=4).grid(column=0, row=6)  
Radiobutton(frm, text='комедия', value=5).grid(column=1, row=6)  
Radiobutton(frm, text='драма', value=6).grid(column=2, row=6)
Radiobutton(frm, text='семейный', value=7).grid(column=3, row=6)  
Radiobutton(frm, text='детектив', value=8).grid(column=4, row=6)  

ttk.Label(frm, text="Ваш самый любимый фильм").grid(column=0, row=8)
num1_en = ttk.Entry(frm)
num1_en.grid(column=1,row=8)

ttk.Label(frm, text="На чем чаще всего вы смотрите фильмы?").grid(column=0, row=9)
combo = Combobox(anketa)
combo['values'] = ('на телефоне','в кино','на телевизоре','на компьютере')
combo.grid(column=0,row=9)

def save():
	Files = [('All Files', '*.*'),
			('Python Files', '*.py'),
			('Text Document', '*.txt')]
	file = asksaveasfile(filetypes = Files, defaultextension = Files)

ttk.Button(frm, text = 'Save', command = lambda : save()).grid(column=3, row=17)
ttk.Button.pack(side = TOP, pady = 20)

anketa.mainloop()
