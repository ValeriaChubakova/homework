# задание 1
s = list (range(1, 10+1))
result = list (map (lambda x: x*2, s))
print('1)', result)

# задание 2
a1 = [1, 2, 3]
a2 = [4, 5, 6]
a3 = [7, 8, 9]
result = [x * y * z for x, y, z in zip(a1, a2, a3)]
print('2)', result)

# задание 3
a1 = ['1','2','3','4']
s = ['lion', a1, 'tiger', 'giraffe', '12']
result = list (map (len, s))
print('3)', result)

# задание 4
s = range (0, 10+1)
result = list (filter (lambda x: x % 2 == 0, s))
print('4)', result)

# задание 5
s = ['cat', 56.32, 123, '', True, [34,23]]
result = list (filter (lambda x: x or x == 0, s))
print('5)', result)

# задание 6
s = [147, 258, 369]
result = (list (zip (s)))
print('6)', result)
     
# задание 7
a1 = [1, 2, 3]
a2 = [4, 5, 6]
result = list (zip (a1, map(lambda x: x * 2, a2)))
print('7)', result)
