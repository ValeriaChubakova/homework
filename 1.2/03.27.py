from tkinter import *
from tkinter import ttk

def summ():
    print(num1_en.get())
    print(num2_en.get())
    result.set(sum(map(int, (num1_en.get(), num2_en.get()))))

root = Tk()
result = StringVar()
frm = ttk.Frame(root, padding=10)
frm.grid()
ttk.Label(frm, text="Сложение двух чисел").grid(column=0, row=0)

ttk.Label(frm, text="Певрое число").grid(column=0, row=1)
num1_en = ttk.Entry(frm)
num1_en.grid(column=1,row=1)

ttk.Label(frm, text="Второе число").grid(column=0, row=2)
num2_en = ttk.Entry(frm)
num2_en.grid(column=1, row=2)


ttk.Button(frm, text="Сложить", command=summ).grid(column=0, row=3)
ttk.Label(frm, text="Ответ: ", textvariable=result).grid(column=1, row=3)
root.mainloop()
