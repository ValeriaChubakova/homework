from turtle import *
pen = Turtle()
def ship():
    left(90)
    forward(400)
    right(90)
    for _ in range(2):
        forward(50)
        right(90)
        forward(50)
        left(90)
	
    right(180)
    forward(50)
    left(90)
    forward(50)
    left(90)
    forward(100)
    left(90)
    left(180)
    for _ in range(2):
        forward(50)
        left(90)
        forward(50)
        right(90)
	
    forward(50)
    right(90)
    forward(50)
    left(90)
    forward(50)
    left(90)
    left(180)
    forward(100)
    for _ in range(2):
        left(90)
        forward(50)

	
    forward(200)
    left(90)
    for _ in range(2):
        forward(50)
        right(90)
	
    forward(100)
    for _ in range(2):
        right(90)
        forward(50)
        left(90)
        forward(50)

    right(90)
    forward(500)
    for _ in range(2):
        right(90)
        forward(50)
        left(90)
        forward(50)
	
    right(90)
    forward(100)
    right(90)
    forward(50)
    right(90)
    forward(50)
    left(90)
    forward(250)

pen.home()
pen.backward(100)
pen.clear()
update()
tracer(0, 0)
for i in range(1000):
    ship()
    penup()
    forward(1)
    pendown()
    update()
    clear()
